import math

def nCr(n: int, r: int) -> int:
    f = math.factorial
    return f(n) // f(r) // f(n - r)

def inputInteger(msg: str) -> int:
    while True:
        n = input(msg)
        if n.isdigit():
            return int(n)

if __name__ == '__main__':
    n = inputInteger('Combiens de marches à l’escalier ? ')
    total = 0
    for n3 in range(n//3 + 1):
        for n2 in range((n - n3*3)//2 + 1):
            n1 = n - n2*2 - n3*3
            m = n1 + n2 + n3
            total += nCr(m, n1)*nCr(m - n1, n2)
    print(f'Il y a {total} façon(s) de monter l’escalier \
en groupes de 1, 2 et 3 marches.')
